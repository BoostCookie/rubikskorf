#![feature(test)]

#[cfg(test)]
mod tests;

use std::fmt;
use std::boxed::Box;
use std::alloc::{alloc, Layout};

const N_EDGES: usize = 12;
const N_CORNERS: usize = 8;

const N_MOVES: usize = 18;
const N_CORNER_ORIENTATIONS: usize = 2187;      // 3^7
const N_EDGE_ORIENTATIONS: usize = 2048;        // 2^11
const N_CORNER_PERMUTATIONS: usize = 40320;     // 8!
const N_HALF_EDGES_POSITIONS: usize = 665280;   // 12! / 6!

// corner indices
const URF: usize = 0;
const UFL: usize = 1;
const ULB: usize = 2;
const UBR: usize = 3;
const DFR: usize = 4;
const DLF: usize = 5;
const DBL: usize = 6;
const DRB: usize = 7;

// edge indices
const UR: usize = 0;
const UF: usize = 1;
const UL: usize = 2;
const UB: usize = 3;
const DR: usize = 4;
const DF: usize = 5;
const DL: usize = 6;
const DB: usize = 7;
const FR: usize = 8;
const FL: usize = 9;
const BL: usize = 10;
const BR: usize = 11;

fn permute<T>(slice: &mut [T], perm: &[usize]) {
    // Example: perm = &[4, 9, 2]
    // Element 4 goes to 9, 9 goes to 2, and 2 goes to 4
    let temp = perm[0];
    for i in &perm[1..] {
        slice.swap(temp, *i);
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Move {
    F,
    F2,
    Fi,
    R,
    R2,
    Ri,
    U,
    U2,
    Ui,
    L,
    L2,
    Li,
    D,
    D2,
    Di,
    B,
    B2,
    Bi,
}

impl fmt::Display for Move {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Move::F => write!(f, "F"),
            Move::F2 => write!(f, "F2"),
            Move::Fi => write!(f, "F'"),
            Move::R => write!(f, "R"),
            Move::R2 => write!(f, "R2"),
            Move::Ri => write!(f, "R'"),
            Move::U => write!(f, "U"),
            Move::U2 => write!(f, "U2"),
            Move::Ui => write!(f, "U'"),
            Move::L => write!(f, "L"),
            Move::L2 => write!(f, "L2"),
            Move::Li => write!(f, "L'"),
            Move::D => write!(f, "D"),
            Move::D2 => write!(f, "D2"),
            Move::Di => write!(f, "D'"),
            Move::B => write!(f, "B"),
            Move::B2 => write!(f, "B2"),
            Move::Bi => write!(f, "B'"),
        }
    }
}

impl Move {
    pub fn from_str(str_repr: &str) -> Move {
        match str_repr {
            "F" => Move::F,
            "F2" => Move::F2,
            "F'" => Move::Fi,
            "R" => Move::R,
            "R2" => Move::R2,
            "R'" => Move::Ri,
            "U" => Move::U,
            "U2" => Move::U2,
            "U'" => Move::Ui,
            "L" => Move::L,
            "L2" => Move::L2,
            "L'" => Move::Li,
            "D" => Move::D,
            "D2" => Move::D2,
            "D'" => Move::Di,
            "B" => Move::B,
            "B2" => Move::B2,
            "B'" => Move::Bi,
            _ => panic!("Invalid move string: {}", str_repr),
        }
    }

    pub fn list_from_str(str_repr: &str) -> Vec<Move> {
        let mut moves = vec![];
        for move_str in str_repr.trim().split(" ") {
            moves.push(Move::from_str(move_str));
        }
        moves
    }

    pub fn str_from_list(moves: &[Move]) -> String {
        let mut res = String::new();
        for cmove in moves {
            res.push_str(&format!("{} ", cmove))
        }
        res.pop();
        res
    }

    fn list() -> [Move; N_MOVES] {
        [Move::F, Move::F2, Move::Fi, Move::R, Move::R2, Move::Ri,
         Move::U, Move::U2, Move::Ui, Move::L, Move::L2, Move::Li,
         Move::D, Move::D2, Move::Di, Move::B, Move::B2, Move::Bi]
    }

    pub fn inverse(self) -> Move {
        match self {
            Move::F => Move::Fi,
            Move::F2 => Move::F2,
            Move::Fi => Move::F,
            Move::R => Move::Ri,
            Move::R2 => Move::R2,
            Move::Ri => Move::R,
            Move::U => Move::Ui,
            Move::U2 => Move::U2,
            Move::Ui => Move::U,
            Move::L => Move::Li,
            Move::L2 => Move::L2,
            Move::Li => Move::L,
            Move::D => Move::Di,
            Move::D2 => Move::D2,
            Move::Di => Move::D,
            Move::B => Move::Bi,
            Move::B2 => Move::B2,
            Move::Bi => Move::B,
        }
    }

    pub fn increment(&mut self) -> bool {
        match self {
            Move::F => *self = Move::F2,
            Move::F2 => *self = Move::Fi,
            Move::Fi => { *self = Move::R; return true; },
            Move::R => *self = Move::R2,
            Move::R2 => *self = Move::Ri,
            Move::Ri => { *self = Move::U; return true; },
            Move::U => *self = Move::U2,
            Move::U2 => *self = Move::Ui,
            Move::Ui => { *self = Move::L; return true; },
            Move::L => *self = Move::L2,
            Move::L2 => *self = Move::Li,
            Move::Li => { *self = Move::D; return true; },
            Move::D => *self = Move::D2,
            Move::D2 => *self = Move::Di,
            Move::Di => { *self = Move::B; return true; },
            Move::B => *self = Move::B2,
            Move::B2 => *self = Move::Bi,
            Move::Bi => { *self = Move::F; return true; },
        }
        false
    }

    pub fn increment_face(&mut self) -> bool {
        match self {
            Move::F => *self = Move::R,
            Move::R => *self = Move::U,
            Move::U => *self = Move::L,
            Move::L => *self = Move::D,
            Move::D => *self = Move::B,
            Move::B => { *self = Move::F; return true; },
            Move::F2 => *self = Move::R,
            Move::Fi => *self = Move::R,
            Move::R2 => *self = Move::U,
            Move::Ri => *self = Move::U,
            Move::U2 => *self = Move::L,
            Move::Ui => *self = Move::L,
            Move::L2 => *self = Move::D,
            Move::Li => *self = Move::D,
            Move::D2 => *self = Move::B,
            Move::Di => *self = Move::B,
            Move::B2 => { *self = Move::F; return true },
            Move::Bi => { *self = Move::F; return true },
        }
        false
    }

    pub fn as_clockwise(self) -> Move {
        match self {
            Move::F => Move::F,
            Move::F2 => Move::F,
            Move::Fi => Move::F,
            Move::R => Move::R,
            Move::R2 => Move::R,
            Move::Ri => Move::R,
            Move::U => Move::U,
            Move::U2 => Move::U,
            Move::Ui => Move::U,
            Move::L => Move::L,
            Move::L2 => Move::L,
            Move::Li => Move::L,
            Move::D => Move::D,
            Move::D2 => Move::D,
            Move::Di => Move::D,
            Move::B => Move::B,
            Move::B2 => Move::B,
            Move::Bi => Move::B,
        }
    }

    pub fn mirror_clockwise(self) -> Move {
        match self {
            Move::F => Move::B,
            Move::R => Move::L,
            Move::U => Move::D,
            Move::L => Move::R,
            Move::D => Move::U,
            Move::B => Move::F,
            Move::F2 => Move::B,
            Move::Fi => Move::B,
            Move::R2 => Move::L,
            Move::Ri => Move::L,
            Move::U2 => Move::D,
            Move::Ui => Move::D,
            Move::L2 => Move::R,
            Move::Li => Move::R,
            Move::D2 => Move::U,
            Move::Di => Move::U,
            Move::B2 => Move::F,
            Move::Bi => Move::F,
        }
    }
}

#[derive(Debug, PartialEq, PartialOrd, Copy, Clone)]
enum CornerName {
    URF,
    UFL,
    ULB,
    UBR,
    DFR,
    DLF,
    DBL,
    DRB,
}

impl CornerName {
    fn rev_list() -> Vec<CornerName> {
        vec![CornerName::DRB, CornerName::DBL, CornerName::DLF, CornerName::DFR,
             CornerName::UBR, CornerName::ULB, CornerName::UFL, CornerName::URF]
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
struct CornerCubie {
    name: CornerName,
    orientation: u8, // 0, 1, or 2
}

impl CornerCubie {
    fn new(name: CornerName, orientation: u8) -> CornerCubie {
        CornerCubie {
            name: name,
            orientation: orientation,
        }
    }

    fn twist(&mut self, twist: u8) {
        self.orientation = (self.orientation + twist) % 3;
    }

    fn from_str(str_repr: &str) -> CornerCubie {
        match str_repr {
            "URF" => CornerCubie::new(CornerName::URF, 0),
            "UFL" => CornerCubie::new(CornerName::UFL, 0),
            "ULB" => CornerCubie::new(CornerName::ULB, 0),
            "UBR" => CornerCubie::new(CornerName::UBR, 0),
            "DFR" => CornerCubie::new(CornerName::DFR, 0),
            "DLF" => CornerCubie::new(CornerName::DLF, 0),
            "DBL" => CornerCubie::new(CornerName::DBL, 0),
            "DRB" => CornerCubie::new(CornerName::DRB, 0),
            "FUR" => CornerCubie::new(CornerName::URF, 1),
            "LUF" => CornerCubie::new(CornerName::UFL, 1),
            "BUL" => CornerCubie::new(CornerName::ULB, 1),
            "RUB" => CornerCubie::new(CornerName::UBR, 1),
            "RDF" => CornerCubie::new(CornerName::DFR, 1),
            "FDL" => CornerCubie::new(CornerName::DLF, 1),
            "LDB" => CornerCubie::new(CornerName::DBL, 1),
            "BDR" => CornerCubie::new(CornerName::DRB, 1),
            "RFU" => CornerCubie::new(CornerName::URF, 2),
            "FLU" => CornerCubie::new(CornerName::UFL, 2),
            "LBU" => CornerCubie::new(CornerName::ULB, 2),
            "BRU" => CornerCubie::new(CornerName::UBR, 2),
            "FRD" => CornerCubie::new(CornerName::DFR, 2),
            "LFD" => CornerCubie::new(CornerName::DLF, 2),
            "BLD" => CornerCubie::new(CornerName::DBL, 2),
            "RBD" => CornerCubie::new(CornerName::DRB, 2),
            _ => panic!("Unknown string for CornerCubie: {}", str_repr),
        }
    }
}

impl fmt::Display for CornerCubie {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.orientation {
            0 => match self.name {
                CornerName::URF => write!(f, "URF"),
                CornerName::UFL => write!(f, "UFL"),
                CornerName::ULB => write!(f, "ULB"),
                CornerName::UBR => write!(f, "UBR"),
                CornerName::DFR => write!(f, "DFR"),
                CornerName::DLF => write!(f, "DLF"),
                CornerName::DBL => write!(f, "DBL"),
                CornerName::DRB => write!(f, "DRB"),
            },
            1 => match self.name {
                CornerName::URF => write!(f, "FUR"),
                CornerName::UFL => write!(f, "LUF"),
                CornerName::ULB => write!(f, "BUL"),
                CornerName::UBR => write!(f, "RUB"),
                CornerName::DFR => write!(f, "RDF"),
                CornerName::DLF => write!(f, "FDL"),
                CornerName::DBL => write!(f, "LDB"),
                CornerName::DRB => write!(f, "BDR"),
            },
            2 => match self.name {
                CornerName::URF => write!(f, "RFU"),
                CornerName::UFL => write!(f, "FLU"),
                CornerName::ULB => write!(f, "LBU"),
                CornerName::UBR => write!(f, "BRU"),
                CornerName::DFR => write!(f, "FRD"),
                CornerName::DLF => write!(f, "LFD"),
                CornerName::DBL => write!(f, "BLD"),
                CornerName::DRB => write!(f, "RBD"),
            }
            _ => panic!("Unknown orientation for CornerCubie: {}", self.orientation),
        }
    }
}

#[derive(Debug, PartialEq, PartialOrd, Copy, Clone)]
enum EdgeName {
    UR,
    UF,
    UL,
    UB,
    DR,
    DF,
    DL,
    DB,
    FR,
    FL,
    BL,
    BR,
}

impl EdgeName {
    fn rev_list() -> Vec<EdgeName> {
        vec![EdgeName::BR, EdgeName::BL, EdgeName::FL, EdgeName::FR, EdgeName::DB, EdgeName::DL,
             EdgeName::DF, EdgeName::DR, EdgeName::UB, EdgeName::UL, EdgeName::UF, EdgeName::UR]
    }

    fn half1_list() -> [EdgeName; 6] {
        [EdgeName::UR, EdgeName::UF, EdgeName::UL, EdgeName::UB, EdgeName::DR, EdgeName::DF]
    }

    fn half2_list_rev() -> [EdgeName; 6] {
        [EdgeName::BR, EdgeName::BL, EdgeName::FL, EdgeName::FR, EdgeName::DB, EdgeName::DL]
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
struct EdgeCubie {
    name: EdgeName,
    orientation: u8, // 0 or 1
}

impl EdgeCubie {
    fn new(name: EdgeName, orientation: u8) -> EdgeCubie {
        EdgeCubie {
            name: name,
            orientation: orientation,
        }
    }

    fn flip(&mut self) {
        self.orientation ^= 1;
    }

    fn from_str(str_repr: &str) -> EdgeCubie {
        match str_repr {
            "UR" => EdgeCubie::new(EdgeName::UR, 0),
            "UF" => EdgeCubie::new(EdgeName::UF, 0),
            "UL" => EdgeCubie::new(EdgeName::UL, 0),
            "UB" => EdgeCubie::new(EdgeName::UB, 0),
            "RU" => EdgeCubie::new(EdgeName::UR, 1),
            "FU" => EdgeCubie::new(EdgeName::UF, 1),
            "LU" => EdgeCubie::new(EdgeName::UL, 1),
            "BU" => EdgeCubie::new(EdgeName::UB, 1),
            "DR" => EdgeCubie::new(EdgeName::DR, 0),
            "DF" => EdgeCubie::new(EdgeName::DF, 0),
            "DL" => EdgeCubie::new(EdgeName::DL, 0),
            "DB" => EdgeCubie::new(EdgeName::DB, 0),
            "RD" => EdgeCubie::new(EdgeName::DR, 1),
            "FD" => EdgeCubie::new(EdgeName::DF, 1),
            "LD" => EdgeCubie::new(EdgeName::DL, 1),
            "BD" => EdgeCubie::new(EdgeName::DB, 1),
            "FR" => EdgeCubie::new(EdgeName::FR, 0),
            "FL" => EdgeCubie::new(EdgeName::FL, 0),
            "BL" => EdgeCubie::new(EdgeName::BL, 0),
            "BR" => EdgeCubie::new(EdgeName::BR, 0),
            "RF" => EdgeCubie::new(EdgeName::FR, 1),
            "LF" => EdgeCubie::new(EdgeName::FL, 1),
            "LB" => EdgeCubie::new(EdgeName::BL, 1),
            "RB" => EdgeCubie::new(EdgeName::BR, 1),
            _ => panic!("Unknown string for EdgeCubie: {}", str_repr),
        }
    }
}

impl fmt::Display for EdgeCubie {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.orientation {
            0 => match self.name {
                EdgeName::UR => write!(f, "UR"),
                EdgeName::UF => write!(f, "UF"),
                EdgeName::UL => write!(f, "UL"),
                EdgeName::UB => write!(f, "UR"),
                EdgeName::DR => write!(f, "DR"),
                EdgeName::DF => write!(f, "DF"),
                EdgeName::DL => write!(f, "DL"),
                EdgeName::DB => write!(f, "DB"),
                EdgeName::FR => write!(f, "FR"),
                EdgeName::FL => write!(f, "FL"),
                EdgeName::BL => write!(f, "BL"),
                EdgeName::BR => write!(f, "BR"),
            },
            1 => match self.name {
                EdgeName::UR => write!(f, "RU"),
                EdgeName::UF => write!(f, "FU"),
                EdgeName::UL => write!(f, "LU"),
                EdgeName::UB => write!(f, "RU"),
                EdgeName::DR => write!(f, "RD"),
                EdgeName::DF => write!(f, "FD"),
                EdgeName::DL => write!(f, "LD"),
                EdgeName::DB => write!(f, "BD"),
                EdgeName::FR => write!(f, "RF"),
                EdgeName::FL => write!(f, "LF"),
                EdgeName::BL => write!(f, "LB"),
                EdgeName::BR => write!(f, "RB"),
            },
            _ => panic!("Illegal orientation of EdgeCubie: {}", self.orientation),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct CubieCube {
    corners: [CornerCubie; N_CORNERS],
    edges: [EdgeCubie; N_EDGES],
}

impl fmt::Display for CubieCube {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {}",
            self.corners[0], self.corners[1], self.corners[2], self.corners[3],
            self.corners[4], self.corners[5], self.corners[6], self.corners[7],
            self.edges[0], self.edges[1], self.edges[2], self.edges[3],
            self.edges[4], self.edges[5], self.edges[6], self.edges[7],
            self.edges[8], self.edges[9], self.edges[10], self.edges[11],
        )
    }
}

impl CubieCube {
    pub fn new_solved() -> CubieCube {
        CubieCube {
            corners: [CornerCubie::new(CornerName::URF, 0), CornerCubie::new(CornerName::UFL, 0), CornerCubie::new(CornerName::ULB, 0), CornerCubie::new(CornerName::UBR, 0),
                      CornerCubie::new(CornerName::DFR, 0), CornerCubie::new(CornerName::DLF, 0), CornerCubie::new(CornerName::DBL, 0), CornerCubie::new(CornerName::DRB, 0)],
            edges: [EdgeCubie::new(EdgeName::UR, 0), EdgeCubie::new(EdgeName::UF, 0), EdgeCubie::new(EdgeName::UL, 0), EdgeCubie::new(EdgeName::UB, 0),
                    EdgeCubie::new(EdgeName::DR, 0), EdgeCubie::new(EdgeName::DF, 0), EdgeCubie::new(EdgeName::DL, 0), EdgeCubie::new(EdgeName::DB, 0),
                    EdgeCubie::new(EdgeName::FR, 0), EdgeCubie::new(EdgeName::FL, 0), EdgeCubie::new(EdgeName::BL, 0), EdgeCubie::new(EdgeName::BR, 0)],
        }
    }

    pub fn from_str(str_repr: &str) -> CubieCube {
        let mut corners = [CornerCubie::new(CornerName::URF, 0); N_CORNERS];
        let mut edges = [EdgeCubie::new(EdgeName::UR, 0); N_EDGES];
        let sub_strings = str_repr.trim().split(" ");
        for (i, sub_string) in sub_strings.enumerate() {
            if i < N_CORNERS {
                corners[i] = CornerCubie::from_str(sub_string);
            } else {
                edges[i - N_CORNERS] = EdgeCubie::from_str(sub_string);
            }
        }
        CubieCube {
            corners: corners,
            edges: edges,
        }
    }

    pub fn is_solved(&self) -> bool {
        self == &CubieCube::new_solved()
    }

    fn twist_corners(&mut self, changes: &[(usize, u8)]) {
        for (index, twist) in changes {
            self.corners[*index].twist(*twist);
        }
    }

    fn flip_edges(&mut self, edges_indices: &[usize]) {
        for index in edges_indices {
            self.edges[*index].flip();
        }
    }

    pub fn do_move(&mut self, cmove: Move) {
        match cmove {
            Move::F => { permute(&mut self.corners, &[URF, DFR, DLF, UFL]);
                         self.twist_corners(&[(URF, 1), (UFL, 2), (DFR, 2), (DLF, 1)]);
                         permute(&mut self.edges, &[UF, FR, DF, FL]);
                         self.flip_edges(&[UF, DF, FR, FL]); },
            Move::R => { permute(&mut self.corners, &[URF, UBR, DRB, DFR]);
                         self.twist_corners(&[(URF, 2), (UBR, 1), (DFR, 1), (DRB, 2)]);
                         permute(&mut self.edges, &[UR, BR, DR, FR]);
                         /* no edge flips */ },
            Move::U => { permute(&mut self.corners, &[URF, UFL, ULB, UBR]);
                         /* no corner twists */
                         permute(&mut self.edges, &[UR, UF, UL, UB]);
                         /* no edge flips */ },
            Move::L => { permute(&mut self.corners, &[UFL, DLF, DBL, ULB]);
                         self.twist_corners(&[(UFL, 1), (ULB, 2), (DLF, 2), (DBL, 1)]);
                         permute(&mut self.edges, &[UL, FL, DL, BL]);
                         /* no edge flips */ },
            Move::D => { permute(&mut self.corners, &[DFR, DRB, DBL, DLF]);
                         /* no corner twists */
                         permute(&mut self.edges, &[DR, DB, DL, DF]);
                         /* no edge flips */ },
            Move::B => { permute(&mut self.corners, &[ULB, DBL, DRB, UBR]);
                         self.twist_corners(&[(ULB, 1), (UBR, 2), (DBL, 2), (DRB, 1)]);
                         permute(&mut self.edges, &[UB, BL, DB, BR]);
                         self.flip_edges(&[UB, DB, BL, BR]); },
            Move::F2 => { self.do_moves(&[Move::F; 2]); },
            Move::Fi => { self.do_moves(&[Move::F; 3]); },
            Move::R2 => { self.do_moves(&[Move::R; 2]); },
            Move::Ri => { self.do_moves(&[Move::R; 3]); },
            Move::U2 => { self.do_moves(&[Move::U; 2]); },
            Move::Ui => { self.do_moves(&[Move::U; 3]); },
            Move::L2 => { self.do_moves(&[Move::L; 2]); },
            Move::Li => { self.do_moves(&[Move::L; 3]); },
            Move::D2 => { self.do_moves(&[Move::D; 2]); },
            Move::Di => { self.do_moves(&[Move::D; 3]); },
            Move::B2 => { self.do_moves(&[Move::B; 2]); },
            Move::Bi => { self.do_moves(&[Move::B; 3]); },
        }
    }

    pub fn do_moves(&mut self, moves: &[Move]) {
        for &cmove in moves {
            self.do_move(cmove);
        }
    }

    // Conversions explained in http://kociemba.org/math/coordlevel.htm
    pub fn corner_orientations(&self) -> u16 {
        let mut corner_orientations = 0;
        for corner_cubie in &self.corners[0..N_CORNERS-1] {
            corner_orientations = 3 * corner_orientations + corner_cubie.orientation as u16;
        }
        corner_orientations
    }

    pub fn edge_orientations(&self) -> u16 {
        let mut edge_orientations = 0;
        for edge_cubie in &self.edges[0..N_EDGES-1] {
            edge_orientations = 2 * edge_orientations + edge_cubie.orientation as u16;
        }
        edge_orientations
    }

    pub fn corner_permutation(&self) -> u16 {
        let mut corner_permutation = 0;
        for i in (URF+1..=DRB).rev() {
            let mut s = 0;
            for j in (URF..i).rev() {
                if self.corners[j].name > self.corners[i].name {
                    s += 1;
                }
            }
            corner_permutation = (corner_permutation + s) * i as u16;
        }
        corner_permutation
    }

    pub fn edge_permutation(&self) -> u32 {
        let mut edge_permutation = 0;
        for i in (UR+1..=BR).rev() {
            let mut s = 0;
            for j in (UR..i).rev() {
                if self.edges[j].name > self.edges[i].name {
                    s += 1;
                }
            }
            edge_permutation = (edge_permutation + s) * i as u32;
        }
        edge_permutation
    }

    // positions of the first 6 (UR..=DF) edges
    pub fn edges1_positions(&self) -> u32 {
        let mut coordinates = [0; N_EDGES];
        for i in 0..N_EDGES {
            coordinates[self.edges[i].name as usize] = i;
        }

        let mut edges1_positions = 0;
        let factors = [55440, 5040, 504, 56, 7, 1]; // [11! / 6!, 10! / 6!, 9! / 6!, ...]
        for (i, factor) in factors.iter().enumerate() {
            let mut s = 0;
            for j in i..N_EDGES {
                if coordinates[i] > coordinates[j] {
                    s += 1;
                }
            }
            edges1_positions += s * factor;
        }
        edges1_positions
    }

    // positions of the last 6 (DL..=BR) edges
    pub fn edges2_positions(&self) -> u32 {
        let mut coordinates = [0; N_EDGES];
        for i in 0..N_EDGES {
            coordinates[N_EDGES - 1 - self.edges[i].name as usize] = i;
        }

        let mut edges2_positions = 0;
        let factors = [55440, 5040, 504, 56, 7, 1]; // [11! / 6!, 10! / 6!, 9! / 6!, ...]
        for (i, factor) in factors.iter().enumerate() {
            let mut s = 0;
            for j in i..N_EDGES {
                if coordinates[i] > coordinates[j] {
                    s += 1;
                }
            }
            edges2_positions += s * factor;
        }
        edges2_positions
    }

    pub fn set_corner_orientations(&mut self, mut corner_orientations: u16) {
        let mut sum = 0;
        for mut corner_cubie in self.corners[0..N_CORNERS-1].iter_mut().rev() {
            corner_cubie.orientation = (corner_orientations % 3) as u8;
            sum += corner_cubie.orientation;
            corner_orientations /= 3;
        }

        self.corners[N_CORNERS-1].orientation = (2 * sum) % 3;
    }

    pub fn set_edge_orientations(&mut self, mut edge_orientations: u16) {
        let mut last = 0;
        for mut edge_cubie in self.edges[0..N_EDGES-1].iter_mut().rev() {
            edge_cubie.orientation = (edge_orientations % 2) as u8;
            last ^= edge_cubie.orientation;
            edge_orientations /= 2;
        }
        self.edges[N_EDGES-1].orientation = last;
    }

    pub fn set_corner_permutation(&mut self, mut corner_permutation: u16) {
        let mut lehmer_code = [0; N_CORNERS - 1];
        for i in 2..=N_CORNERS {
            lehmer_code[N_CORNERS - i] = corner_permutation as usize % i;
            corner_permutation /= i as u16;
        }

        let mut corners = CornerName::rev_list();
        for (mut corner_cubie, &lehmer_i) in self.corners.iter_mut().rev().zip(lehmer_code.iter()) {
            corner_cubie.name = corners.remove(lehmer_i);
        }

        self.corners[0].name = corners[0];
    }

    pub fn set_edge_permutation(&mut self, mut edge_permutation: u32) {
        let mut lehmer_code = [0; N_EDGES - 1];
        for i in 2..=N_EDGES {
            lehmer_code[N_EDGES - i] = edge_permutation as usize % i;
            edge_permutation /= i as u32;
        }

        let mut edges = EdgeName::rev_list();
        for (mut edge_cubie, &lehmer_i) in self.edges.iter_mut().rev().zip(lehmer_code.iter()) {
            edge_cubie.name = edges.remove(lehmer_i);
        }

        self.edges[0].name = edges[0];
    }

    // positions of the first 6 (UR..=DF) edges
    // messes up edges2_positions
    pub fn set_edges1_positions(&mut self, mut edges1_positions: u32) {
        let mut lehmer_code = [0; 6];
        for i in 7..=N_EDGES {
            lehmer_code[N_EDGES - i] = edges1_positions as usize % i;
            edges1_positions /= i as u32;
        }

        let mut positions = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        for (&lehmer_i, edge_cubie_name) in lehmer_code.iter().zip(EdgeName::half1_list()) {
            self.edges[positions.remove(lehmer_i)].name = edge_cubie_name;
        }
        for (&pos, edge_cubie_name) in positions.iter().zip(EdgeName::half2_list_rev()) {
            self.edges[pos].name = edge_cubie_name;
        }
    }

    // positions of the last 6 (DL..=BR) edges
    // messes up edges1_positions
    pub fn set_edges2_positions(&mut self, mut edges2_positions: u32) {
        let mut lehmer_code = [0; 6];
        for i in 7..=N_EDGES {
            lehmer_code[N_EDGES - i] = edges2_positions as usize % i;
            edges2_positions /= i as u32;
        }

        let mut positions = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        for (&lehmer_i, edge_cubie_name) in lehmer_code.iter().zip(EdgeName::half2_list_rev()) {
            self.edges[positions.remove(lehmer_i)].name = edge_cubie_name;
        }
        for (&pos, edge_cubie_name) in positions.iter().zip(EdgeName::half1_list()) {
            self.edges[pos].name = edge_cubie_name;
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct CoordinateCube {
    corner_orientations: u16,   // 3^7 possibilities
    edge_orientations: u16,     // 2^11 possibilities
    corner_permutation: u16,    // 8! possibilities
    edges1_positions: u32,      // 12! / 6! possibilities
    edges2_positions: u32,      // 12! / 6! possibilities
}

impl CoordinateCube {
    pub fn from_cubie_cube(cubies: &CubieCube) -> CoordinateCube {
        CoordinateCube {
            corner_orientations: cubies.corner_orientations(),
            edge_orientations: cubies.edge_orientations(),
            corner_permutation: cubies.corner_permutation(),
            edges1_positions: cubies.edges1_positions(),
            edges2_positions: cubies.edges2_positions(),
        }
    }

    pub fn new_solved() -> CoordinateCube {
        CoordinateCube {
            corner_orientations: 0,
            edge_orientations: 0,
            corner_permutation: 0,
            edges1_positions: 0,
            edges2_positions: N_HALF_EDGES_POSITIONS as u32 - 1,
        }
    }

    pub fn is_solved(&self) -> bool {
        self == &CoordinateCube {
            corner_orientations: 0,
            edge_orientations: 0,
            corner_permutation: 0,
            edges1_positions: 0,
            edges2_positions: 665279,
        }
    }

    pub fn do_move(&mut self, move_tables: &MoveTables, cmove: Move) {
        self.corner_orientations = move_tables.corner_orientations[self.corner_orientations as usize][cmove as usize];
        self.edge_orientations = move_tables.edge_orientations[self.edge_orientations as usize][cmove as usize];
        self.corner_permutation = move_tables.corner_permutation[self.corner_permutation as usize][cmove as usize];
        self.edges1_positions = move_tables.half_edges_positions[self.edges1_positions as usize][cmove as usize];
        self.edges2_positions = move_tables.half_edges_positions[self.edges2_positions as usize][cmove as usize];
    }

    pub fn do_moves(&mut self, move_tables: &MoveTables, moves: &[Move]) {
        for &cmove in moves {
            self.do_move(move_tables, cmove);
        }
    }

    pub fn find_solution(&self, move_tables: &MoveTables) -> Vec<Move> {
        if !self.is_solved() {
            for len in 1..=20 {
                let mut moves = Vec::with_capacity(len);
                for cmove in Move::list() {
                    let mut cur_cube = self.clone();
                    cur_cube.do_move(move_tables, cmove);

                    moves.push(cmove);
                    if search_solution(cur_cube, move_tables, len - 1, &mut moves) {
                        return moves;
                    }
                    moves.pop().unwrap();
                }
            }
        }
        return vec![];
    }
}

// Move tables for the CoordinateCube
#[derive(Debug, Clone)]
pub struct MoveTables {
    corner_orientations: Box<[[u16; N_MOVES]; N_CORNER_ORIENTATIONS]>,
    edge_orientations: Box<[[u16; N_MOVES]; N_EDGE_ORIENTATIONS]>,
    corner_permutation: Box<[[u16; N_MOVES]; N_CORNER_PERMUTATIONS]>,
    half_edges_positions: Box<[[u32; N_MOVES]; N_HALF_EDGES_POSITIONS]>,
}

impl MoveTables {
    pub fn build() -> MoveTables {
        let mut cubies = CubieCube::new_solved();

        // allocate
        let layout_co_o = Layout::new::<[[u16; N_MOVES]; N_CORNER_ORIENTATIONS]>();
        let layout_ed_o = Layout::new::<[[u16; N_MOVES]; N_EDGE_ORIENTATIONS]>();
        let layout_co_p = Layout::new::<[[u16; N_MOVES]; N_CORNER_PERMUTATIONS]>();
        let layout_he_p = Layout::new::<[[u32; N_MOVES]; N_HALF_EDGES_POSITIONS]>();
        let mut corner_orientations = unsafe {
            let ptr = alloc(layout_co_o) as *mut [[u16; N_MOVES]; N_CORNER_ORIENTATIONS];
            Box::from_raw(ptr)
        };
        let mut edge_orientations = unsafe {
            let ptr = alloc(layout_ed_o) as *mut [[u16; N_MOVES]; N_EDGE_ORIENTATIONS];
            Box::from_raw(ptr)
        };
        let mut corner_permutation = unsafe {
            let ptr = alloc(layout_co_p) as *mut [[u16; N_MOVES]; N_CORNER_PERMUTATIONS];
            Box::from_raw(ptr)
        };
        let mut half_edges_positions = unsafe {
            let ptr = alloc(layout_he_p) as *mut [[u32; N_MOVES]; N_HALF_EDGES_POSITIONS];
            Box::from_raw(ptr)
        };

        // initialise
        for values in corner_orientations.iter_mut() {
            *values = [u16::MAX; N_MOVES];
        }
        for values in edge_orientations.iter_mut() {
            *values = [u16::MAX; N_MOVES];
        }
        for values in corner_permutation.iter_mut() {
            *values = [u16::MAX; N_MOVES];
        }
        for values in half_edges_positions.iter_mut() {
            *values = [u32::MAX; N_MOVES];
        }

        // corner orientations
        for co in 0..N_CORNER_ORIENTATIONS {
            for cmove in Move::list() {
                if corner_orientations[co][cmove as usize] != u16::MAX {
                    continue;
                }
                cubies.set_corner_orientations(co as u16);
                cubies.do_move(cmove);
                let new_co = cubies.corner_orientations();
                corner_orientations[co][cmove as usize] = new_co;
                corner_orientations[new_co as usize][cmove.inverse() as usize] = co as u16;
            }
        }

        // edge orientations
        for co in 0..N_EDGE_ORIENTATIONS {
            for cmove in Move::list() {
                if edge_orientations[co][cmove as usize] != u16::MAX {
                    continue;
                }
                cubies.set_edge_orientations(co as u16);
                cubies.do_move(cmove);
                let new_co = cubies.edge_orientations();
                edge_orientations[co][cmove as usize] = new_co;
                edge_orientations[new_co as usize][cmove.inverse() as usize] = co as u16;
            }
        }

        // corner permutations
        for co in 0..N_CORNER_PERMUTATIONS {
            for cmove in Move::list() {
                if corner_permutation[co][cmove as usize] != u16::MAX {
                    continue;
                }
                cubies.set_corner_permutation(co as u16);
                cubies.do_move(cmove);
                let new_co = cubies.corner_permutation();
                corner_permutation[co][cmove as usize] = new_co;
                corner_permutation[new_co as usize][cmove.inverse() as usize] = co as u16;
            }
        }

        // positions table for half of the edges
        for co in 0..N_HALF_EDGES_POSITIONS {
            for cmove in Move::list() {
                if half_edges_positions[co][cmove as usize] != u32::MAX {
                    continue;
                }
                cubies.set_edges1_positions(co as u32);
                cubies.do_move(cmove);
                let new_co = cubies.edges1_positions();
                half_edges_positions[co][cmove as usize] = new_co;
                half_edges_positions[new_co as usize][cmove.inverse() as usize] = co as u32;
            }
        }

        MoveTables {
            corner_orientations: corner_orientations,
            edge_orientations: edge_orientations,
            corner_permutation: corner_permutation,
            half_edges_positions: half_edges_positions,
        }
    }
}

fn search_solution(cur_cube: CoordinateCube, move_tables: &MoveTables, remaining_len: usize, moves: &mut Vec<Move>) -> bool {
    // detects useless sequences like U R R and even R L R
    fn last_move_is_useless(moves: &[Move]) -> bool {
        let len = moves.len();
        match len {
            0 => false,
            1 => false,
            2 => moves[0].as_clockwise() == moves[1].as_clockwise(),
            _ => (moves[len - 1] == moves[len - 2].as_clockwise())
                || (moves[len - 1] == moves[len - 3].as_clockwise()
                    && moves[len - 1].mirror_clockwise() == moves[len - 2].as_clockwise()),
        }
    }

    if remaining_len == 0 {
        if cur_cube.is_solved() {
            return true;
        } else {
            return false;
        }
    }

    let mut new_move = Move::F;
    let mut new_face = true;
    loop {
        moves.push(new_move);
        // ignore some unnecessary move sequences
        if new_face && last_move_is_useless(&moves) {
            moves.pop().unwrap();
            if new_move.increment_face() {
                return false;
            }
            continue;
        }

        let mut new_cube = cur_cube.clone();
        new_cube.do_move(move_tables, new_move);
        if search_solution(new_cube, move_tables, remaining_len - 1, moves) {
            return true;
        }

        moves.pop().unwrap();
        new_face = new_move.increment();
        if new_face && new_move == Move::F {
            return false;
        }
    }
}


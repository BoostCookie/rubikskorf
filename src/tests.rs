extern crate test;

use super::*;
use test::Bencher;

#[test]
fn scramble() {
    let mut cube = CubieCube::new_solved();
    let moves = Move::list_from_str("D' F2 D' F R2 B2 U' D L U' F D2 B' R' D L2 D2 B L B U' L2 F' B2 D2 R' L F' L B2 U' R2 B U F D' R D L2 D' R2 B2 U' L2 U");
    cube.do_moves(&moves);
    assert_eq!(&cube, &CubieCube::new_solved());
}

#[test]
fn corner_orientations() {
    let mut cube = CubieCube::new_solved();
    cube.set_corner_orientations(1494);
    assert_eq!(cube.corner_orientations(), 1494);
    assert_eq!(cube.corners[0].orientation, 2);
    assert_eq!(cube.corners[1].orientation, 0);
    assert_eq!(cube.corners[2].orientation, 0);
    assert_eq!(cube.corners[3].orientation, 1);
    assert_eq!(cube.corners[4].orientation, 1);
    assert_eq!(cube.corners[5].orientation, 0);
    assert_eq!(cube.corners[6].orientation, 0);
    assert_eq!(cube.corners[7].orientation, 2);
    cube = CubieCube::new_solved();
    cube.do_move(Move::R);
    assert_eq!(cube.corner_orientations(), 1494);
    assert_eq!(cube.corners[0].orientation, 2);
    assert_eq!(cube.corners[1].orientation, 0);
    assert_eq!(cube.corners[2].orientation, 0);
    assert_eq!(cube.corners[3].orientation, 1);
    assert_eq!(cube.corners[4].orientation, 1);
    assert_eq!(cube.corners[5].orientation, 0);
    assert_eq!(cube.corners[6].orientation, 0);
    assert_eq!(cube.corners[7].orientation, 2);
}

#[test]
fn edge_orientations() {
    let mut cube = CubieCube::new_solved();
    cube.set_edge_orientations(42);
    assert_eq!(cube.edge_orientations(), 42);
    for cmove in Move::list() {
        cube.do_move(cmove);
        let coord = cube.edge_orientations();
        cube = CubieCube::new_solved();
        cube.set_edge_orientations(coord);
        cube.do_move(cmove.inverse());
        assert_eq!(cube.edge_orientations(), 42);
    }
}

#[test]
fn corner_permutation() {
    let mut cube = CubieCube::new_solved();
    cube.set_corner_permutation(21021);
    assert_eq!(cube.corner_permutation(), 21021);
    cube = CubieCube::new_solved();
    cube.do_move(Move::R);
    assert_eq!(cube.corner_permutation(), 21021);
}

#[test]
fn edge_permutation() {
    let mut cube = CubieCube::new_solved();
    cube.set_edge_permutation(412345678);
    assert_eq!(cube.edge_permutation(), 412345678);
}

#[test]
fn edges_positions() {
    let mut cube = CubieCube::new_solved();
    cube.set_edges1_positions(275398);
    assert_eq!(cube.edges1_positions(), 275398);
    cube.set_edges2_positions(275398);
    assert_eq!(cube.edges2_positions(), 275398);
    for cmove in Move::list() {
        cube.set_edges1_positions(275398);
        cube.do_move(cmove);
        let pos_coord = cube.edges1_positions();
        cube.set_edges2_positions(275398);
        cube.do_move(cmove);
        assert_eq!(cube.edges2_positions(), pos_coord);
    }
    let moves = Move::list_from_str("D' F2 D' F R2 B2 U' D L U' F D2 B' R' D L2 D2 B L B U' L2 F' B2 D2");
    cube.set_edges1_positions(123456);
    cube.do_moves(&moves);
    let pos_coord = cube.edges1_positions();
    cube.set_edges2_positions(123456);
    cube.do_moves(&moves);
    assert_eq!(cube.edges2_positions(), pos_coord);
}

#[test]
fn move_tables() {
    let mut cubies = CubieCube::new_solved();
    let start_moves = Move::list_from_str("D' F2 D' F R2 B2 U' D L U' F D2 B' R' D L2 D2 B L B U' L2 F' B2 D2");
    cubies.do_moves(&start_moves);
    let mut coords = CoordinateCube::from_cubie_cube(&cubies);
    let move_tables = MoveTables::build();
    let test_moves = Move::list_from_str("D2 L D L' D' F L B' R2 U' F R B L2 F L' F R' F R2 U' F D F' R");
    cubies.do_moves(&test_moves);
    coords.do_moves(&move_tables, &test_moves);
    assert_eq!(coords, CoordinateCube::from_cubie_cube(&cubies));
}

#[test]
fn find_solution5() {
    let mut cube = CoordinateCube::new_solved();
    let move_tables = MoveTables::build();
    let scramble = Move::list_from_str("U R' B2 L' R2");
    cube.do_moves(&move_tables, &scramble);
    assert_eq!(cube.find_solution(&move_tables), Move::list_from_str("R2 L B2 R U'"));
}

#[bench]
fn bench_find_solution5(b: &mut Bencher) {
    let mut cube = CoordinateCube::new_solved();
    let move_tables = MoveTables::build();
    cube.do_moves(&move_tables, &Move::list_from_str("D R' B U2 L"));
    b.iter({move ||
        cube.find_solution(&move_tables)
    })
}

/*
#[bench]
fn bench_build_tables(b: &mut Bencher) {
    b.iter({|| MoveTables::build()})
}
*/

#[bench]
fn bench_set_corner_orientations(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    b.iter(move || {
        for i in 0..2048 {//2187 {
            let k = test::black_box(i);
            cube.set_corner_orientations(k);
        }
    })
}

#[bench]
fn bench_get_corner_orientations(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    cube.set_corner_orientations(2000);
    b.iter(move || {
        for _ in 0..2048 {//2187 {
            let _n = test::black_box(cube.corner_orientations());
        }
    })
}

#[bench]
fn bench_set_edge_orientations(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    b.iter(move || {
        for i in 0..2048 {
            cube.set_edge_orientations(test::black_box(i));
        }
    })
}

#[bench]
fn bench_get_edge_orientations(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    cube.set_edge_orientations(2000);
    b.iter(move || {
        for _ in 0..2048 {
            let _n = test::black_box(cube.edge_orientations());
        }
    })
}

#[bench]
fn bench_set_corner_permutation(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    b.iter(move || {
        for i in (0..40320).step_by(40320 / 2048) {
            cube.set_corner_permutation(i);
        }
    })
}

#[bench]
fn bench_get_corner_permutation(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    cube.set_corner_permutation(40123);
    b.iter(move || {
        for _ in 0..2048 {
            cube.corner_permutation();
        }
    })
}

#[bench]
fn bench_set_edge_permutation(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    b.iter(move || {
        for i in (0..479001600).step_by(479001600 / 2048) {
            cube.set_edge_permutation(i);
        }
    })
}

#[bench]
fn bench_get_edge_permutation(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    cube.set_edge_permutation(432101234);
    b.iter(move || {
        for _ in 0..2048 {
            cube.edge_permutation();
        }
    })
}

#[bench]
fn bench_set_edges1_positions(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    b.iter(move || {
        for i in (0..665280).step_by(665280 / 2048) {
            cube.set_edges1_positions(i);
        }
    })
}

#[bench]
fn bench_get_edges1_positions(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    cube.set_edges1_positions(628413);
    b.iter(move || {
        for _ in 0..2048 {
            cube.edges1_positions();
        }
    })
}

#[bench]
fn bench_set_edges2_positions(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    b.iter(move || {
        for i in (0..665280).step_by(665280 / 2048) {
            cube.set_edges2_positions(i);
        }
    })
}

#[bench]
fn bench_get_edges2_positions(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    cube.set_edges2_positions(628413);
    b.iter(move || {
        for _ in 0..2048 {
            cube.edges2_positions();
        }
    })
}

#[bench]
fn bench_scramble_cubies(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    let moves = Move::list_from_str("D' F2 D' F R2 B2 U' D L U' F D2 B' R' D L2 D2 B L B U' L2 F' B2 D2");
    b.iter(move || {
        cube.do_moves(&moves);
    });
}

#[bench]
fn bench_scramble_coords(b: &mut Bencher) {
    let mut cube = CoordinateCube::new_solved();
    let move_tables = MoveTables::build();
    let moves = Move::list_from_str("D' F2 D' F R2 B2 U' D L U' F D2 B' R' D L2 D2 B L B U' L2 F' B2 D2");
    b.iter(move || {
        cube.do_moves(&move_tables, &moves);
    });
}

#[bench]
fn bench_scramble_inv(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    let moves = Move::list_from_str("D' F' D' F' R' B' U' D' L' U' F' D' B' R' D' L' D' B' L' B' U' L' F' B' D'");
    b.iter(move || {
        cube.do_moves(&moves);
    });
}

#[bench]
fn bench_scramble_forw(b: &mut Bencher) {
    let mut cube = CubieCube::new_solved();
    let moves = Move::list_from_str("D F D F R B U D L U F D B R D L D B L B U L F B D");
    b.iter(move || {
        cube.do_moves(&moves);
    });
}

#[bench]
fn bench_new_cube(b: &mut Bencher) {
    b.iter(|| {
        for i in 0..25 {
            let _n = test::black_box(i);
            if i > 25 {
                continue;
            }
            let _c = test::black_box(CubieCube::new_solved());
        }
    });
}
